# stdlib
import argparse
import json
import time

# external deps
import matrix_client_core as botcore
import matrix_client_core.nocurses as nocurses
import matrix_client_core.notifier as notifier
import transformat.parser

# in-tree deps
from language import English as lang
import question

DEBUG=False


class Config(dict):
	def __init__(self, filename="config.json"):
		dict.__init__(self)
		self.filename = filename
		self._set_defaults()

	def _set_defaults(self):
		self['accountfile'] = "account.json"
		self['initial_sync_timeout_seconds'] = 600
		self['orgfiles'] = ["builtin-questions.org"]
		self['prefixes'] = ['FAQBot: ']
		self['masters'] = []
		self['send_sleep_time'] = 5

	def loadfromfile(self, filename=None):
		# Load self from filename.
		if filename is None: filename = self.filename
		self.clear()
		self._set_defaults()
		self.update(self._load(filename))

	def _load(self, filename):
		# Load json file and return as decoded json.
		with open(filename, "r") as f:
			return json.load(f)

	def savetofile(self, filename=None):
		if filename is None: filename = self.filename
		with open(filename, "w") as f:
			json.dump(self, f, sort_keys=True, indent="\t", separators=(',', ': '))


class NotificationPrinter(notifier.NotificationListener):
	@staticmethod
	def on_mcc_mxc_sendrunner_sendcmd(service, event, data):
		nocurses.print("Sending message:", data, fg='red')


class FAQBot(botcore.MXClient):
	def __init__(self, config):
		self.config = config
		self.followups = {}

		self.questionhandler = question.QuestionHandler()
		for fn in self.config['orgfiles']:
			self.questionhandler.add_org_file(fn)

		# prevent delay before replying to first question
		self.questionhandler.generate_index()

		botcore.MXClient.__init__(self, self.config['accountfile'])
		self.send_sleep_time = self.config['send_sleep_time']

	def _strip_prefix(self, txt):
		for p in self.config['prefixes']:
			if txt.startswith(p):
				print("Prefix stripped:", repr(txt[len(p):]))
				return txt[len(p):]
		return None

	def get_followup_reply(self, room_id, user_id, *texts):
		try: ctime, resobj = self.followups[room_id + user_id]
		except KeyError: return None
		now = time.time()
		if now - ctime > 600:
			del self.followups[room_id + user_id]
			return None
		for text in texts:
			if text is None: continue
			reply = resobj.match_followup(text)
			if reply is not None: return reply
		return None

	def add_followup_if_necessary(self, room_id, user_id, response):
		if not response.has_followup(): return
		self.followups[room_id + user_id] = (time.time(), response)

	def send_formatted_messages(self, room_id, messages):
		# Send out a series of formatted messages
		# messages: an iterable of org-mode formatted chunks of text

		for line in messages:
			parsed_line = transformat.parser.OrgElementParser.parse(line)
			plain_line = parsed_line.as_text()
			html_line = parsed_line.as_html()
			if (plain_line == html_line):
				self.sendmsg(room_id, plain_line)
			else:
				self.sendmsg(room_id, (plain_line, html_line))

	@botcore.wrap_exception
	def on_m_room_message(self, event):
		self.last_event = event
		if not event['content']['msgtype'] == "m.text": return

		roomid = event['room_id']
		sender = event['sender']
		inmsg = event['content']['body']

		unprefixedmsg = self._strip_prefix(inmsg)

		reply = self.get_followup_reply(roomid, sender, inmsg, unprefixedmsg)
		if reply:
			notifier.notify(__file__, 'faqbot.match.followup', event)
			self.send_formatted_messages(roomid, reply)
			return

		response = self.questionhandler.ask(inmsg, fast_only=True, user=sender)
		if response:
			answer = response.reply
			notifier.notify(__file__, 'faqbot.match.question', event)
			self.send_formatted_messages(roomid, answer)
			return

		if unprefixedmsg is None: return

		notifier.notify(__file__, 'faqbot.match.prefix', event)
		question = unprefixedmsg
		response = self.questionhandler.ask(question, user=sender)
		if response:
			answer = response.reply
			self.send_formatted_messages(roomid, answer)
			self.add_followup_if_necessary(roomid, sender, response)
		elif lang.is_question(inmsg):
			self.sendmsg(roomid, "{}: I don't know.".format(sender))
		elif sender in self.config['masters']:
			self.sendmsg(roomid, "Master?")
		else:
			self.sendmsg(roomid, "{}: I'm afraid I don't understand you. Try asking a question.".format(sender))

	def connect(self):
		self.sync_filter = '''{
			"presence": { "types": [ "" ] },
			"room": {
				"ephemeral": { "types": [ "" ] },
				"state": {
					"types": [
						"m.room.canonical_alias",
						"m.room.aliases",
						"m.room.name",
						"m.room.topic"
					]
				},
				"timeline": {
					"types": [ "*" ],
					"limit": 3
				}
			}
		}'''

		self.is_bot = True
		self.login()
		self.first_sync()
		self.sdkclient.add_listener(self.on_m_room_message, 'm.room.message')
		self.start_send_thread(self.on_send_message)
		self.hook()

	@botcore.wrap_exception
	def on_send_message(self, room_id, msg):
		if isinstance(msg, str): return self.sdkclient.api.send_notice(room_id, msg)

		# matrix-python-sdk is not that helpful yet. We have to roll our own here.

		content = {
			"msgtype": "m.notice",
			"body": msg[0],
			"formatted_body": msg[1],
			"format": "org.matrix.custom.html"
		}

		self.sdkclient.api.send_message_event(room_id, "m.room.message", content)

	def run_forever(self):
		print("Ready.")
		self.repl()


def parse_args():
	parser = argparse.ArgumentParser(description="FAQBot is a Matrix bot that answers questions.")
	parser.add_argument("--write-config", action='store_true', default=False, help="Write a default configuration file and exit. WARNING: Will overwrite any existing configuration file!")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	options = parser.parse_args()
	return options

def main():
	cmdargs = parse_args()
	global DEBUG
	DEBUG = cmdargs.debug

	cfg = Config()
	if cmdargs.write_config:
		cfg.savetofile()
		return
	cfg.loadfromfile()

	np = NotificationPrinter()

	fb = FAQBot(cfg)
	fb.connect()
	fb.run_forever()

if __name__ == '__main__':
	try: import site_config
	except ImportError: pass

	main()
