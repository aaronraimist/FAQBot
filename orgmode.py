#!/usr/bin/env python3

# Org-mode format handling
# stdlib
import re


class OrgException(Exception):
	pass


class OrgNode:
	def __init__(self, text):
		self.prev_node = None
		self.next_node = None
		self.parent_node = None
		self.child_node = None
		self.level = None
		self.text = text

	def __repr__(self):
		txt = "OrgNode(%s)" % self.text
		if self.level is not None:
			txt = "%s (lvl: %d)" % (txt, self.level)
		return txt

	def root(self):
		# Find the root node
		rootnode = self
		while rootnode.parent_node is not None:
			rootnode = rootnode.parent_node
		while rootnode.prev_node is not None:
			rootnode = rootnode.prev_node
		return rootnode

	def next(self):
		# Walk one step
		node = self

		if node.child_node is not None:
			return node.child_node
		elif node.next_node is not None:
			return node.next_node
		else:
			while node.parent_node is not None:
				node = node.parent_node
				if node.next_node is not None:
					return node.next_node
		return None

	def __lt__(self, other):
		return self.text < other.text

	def __le__(self, other):
		return self.text <= other.text

	def __gt__(self, other):
		return self.text > other.text

	def __ge__(self, other):
		return self.text >= other.text


class HeadlineNode(OrgNode):
	def __init__(self, text):
		OrgNode.__init__(self, text)
		self.level = self.__count_level(text)
		self.text = text[self.level:].strip()
		self.data = []
		self.bodytext = []
		self.tags = ()
		self.properties = {}
		self.parser_adding_bodytext = False

	@staticmethod
	def __count_level(text):
		level = 0
		try:
			while text[level] == "*":
				level += 1
		except IndexError:
			# End of string. Clearly not an asterisk. No special handling needed.
			pass
		return level

	@classmethod
	def try_parse(cls, text):
		m = ParseTools.HEADLINE.search(text)
		if not m: return None
		ilvl = len(m.group(1))
		thetext = m.group(2)
		anchor = m.group(3)
		tags = m.group(4)
		#print("Tags:", repr(tags))
		if (tags): tags = tags.split(":")

		newobj = cls(thetext)
		newobj.level = ilvl
		if tags: newobj.tags = tags
		if anchor: newobj.properties["custom_id"] = anchor
		return newobj

	def add_data(self, line):
		if self.parser_adding_bodytext: return self.addbodytext(line)
		if line == "":
			self.parser_adding_bodytext = True
			return None
		return self.data.append(line)

	def addbodytext(self, text):
		if text == "": return None
		return self.bodytext.append(text)

	def parse_properties(self):
		inprops = False
		for line in self.data:
			if inprops:
				#print(line)
				if ParseTools.PROPEND.search(line):
					inprops = False
					continue
				m = ParseTools.PROPVAL.search(line)
				if m:
					#print("!")
					self.properties[m.group(1).lower()] = m.group(2)
			elif ParseTools.PROPSTART.search(line):
				inprops = True

	def finalize(self):
		# We're done adding data to this object. Do final parsing.
		self.parse_properties()

		if __name__ == '__main__':
			print("Finalizing Object:")
			print(">", self.level, self.text)
			for line in self.data: print("  (data)", line)
			for line in self.bodytext: print("  (body)", line)
			for k, v in self.properties.items(): print("  (prop)", k, "=", v)

	def attach(self, other):
		# Attach (append) ourselves to other
		if self.level == other.level:
			# We are peer
			self.prev_node = other
			other.next_node = self
			self.parent_node = other.parent_node
		elif self.level == other.level + 1:
			# We are child
			self.parent_node = other
			other.child_node = self
		elif self.level < other.level:
			# Our level is higher than other. Cannot attach here.
			# Go back up the chain and find suitable parent.
			while other.level > self.level:
				if other.parent_node is not None:
					other = other.parent_node
				else:
					raise OrgException("No suitable level found for attaching %s to %s" %
						(repr(self), repr(other)))
			self.prev_node = other
			other.next_node = self
			self.parent_node = other.parent_node
		else:
			raise OrgException("Something weird happened attaching %s to %s. Skipped a level?" %
				(repr(self), repr(other)))


class ParseTools:
	HEADLINE = re.compile("^(\\*+)\s+(.*?)(?:\\s+@@html:<a\\s+name=\"([A-Za-z0-9_-]+)\">@@)?(?:\\s+[:]([A-Za-z0-9_:-]+)[:])?$")
	PROPSTART = re.compile("^:PROPERTIES:$")
	PROPVAL = re.compile("^:([A-Za-z0-9_-]+):(?:\\s*(\\S+))?$")
	PROPEND = re.compile("^:END:$")


class TagCollection:
	# Collect entries by tags
	def __init__(self):
		self.tags = {}

	def add(self, tag, obj):
		if tag not in self.tags:
			self.tags[tag] = []
		if obj not in self.tags[tag]: # hopefully does the right thing
			self.tags[tag].append(obj)


class OrgDocument:
	def __init__(self):
		self.rootnode = None
		self.lastnode = None

	def readfromfile(self, filename):
		with open(filename) as f:
			return self.readfromfileobj(f)

	def readfromfileobj(self, fileobj):
		preamble = True
		emptyline = True
		hl = self.lastnode
		while True:
			new_node_opportunity = emptyline
			l = fileobj.readline()
			if l == "": break

			l = l.rstrip()
			emptyline = (l == "")

			if new_node_opportunity and not emptyline:
				hl2 = HeadlineNode.try_parse(l)
				if hl2 is not None:
					preamble = False
					if self.rootnode is None: self.rootnode = hl2
					self.lastnode = hl2
					if hl is not None:
						hl.finalize()
						hl2.attach(hl)
					hl = hl2
					emptyline = True
					continue
			if hl and not preamble:
				hl.add_data(l)

		if hl: hl.finalize()

	def walk(self, origin=None):
		node = origin
		if node is None: node = self.rootnode

		while node:
			yield node
			node = node.next()
